var mongo = require('mongodb');
var async = require('async');

/*
mongo.connect('mongodb://tetris:test@ds033457.mongolab.com:33457/tetris', {}, function(error, db) {
  db.collection('test', function(err, collection) {
   collection.remove({}, function(err, result) {
      for(var i = 0; i < 3; i++) {
        collection.insert({'b':i});
      }
      db.close(); 
    });
  });
});
*/

async.series(
  [
    function(callback) {
      mongo.connect('mongodb://tetris:test@ds033457.mongolab.com:33457/tetris', {}, function(error, db) {
        DB = db; 
        DB.collection('test', function(err, collection) {
          COLLECTION = collection;
          callback(null, 1);
        });
      });
    },
    function(callback) {
      COLLECTION.remove({}, function(err, result) {
        callback(null, 2);  
      });
    },
    function(callback) {
      for(var i = 0; i < 3; i++) {
        COLLECTION.insert({'c':i});
      }
      callback(null, 3);
    },
    function(callback) {
      DB.close();
      callback(null, 4);
    }
  ],
  function(err, results) {
    // default callback  
  }
);
