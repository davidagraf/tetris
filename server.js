var express = require('express');
var app = express.createServer();

var PORT = process.env.C9_PORT || process.env['app_port'] || 3000;

// express

app.configure(function () {
    app.use(express.static(__dirname + '/public'));
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.get('/test', function (req, res) {
    res.send('Hello World');
});

app.listen(PORT);

console.log('Server running at http://127.0.0.1:' + PORT + '/');

// socket.io

var io = require('socket.io').listen(app);
var users = {};

var abolishSocket = io
  .of('/abolish')
  .on('connection', function (socket) {
    socket.emit('who are you', { 'id' : socket.id } );
    socket.on('check in', function (data) {
      users[socket.id] = data.name;
    });
    socket.on('decreased', function(data) {
      // TODO decrement amount
      console.log(data);
      abolishSocket.emit(
        'someone decreased', 
        { 'id' : socket.id,
          'name' : users[socket.id],
          'amount' : data.amount }
      );
    });
  });
