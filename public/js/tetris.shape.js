var SHAPE_SQUARE_WIDTH = 4;
var TURN_LEFT = 0;
var TURN_RIGHT = 1;

var Shape = function () {
  var _this = this;
  var _shapes = [];
  var _squares = [];
  var _currentStateId = 0;

  this.base = function (color) {
    _this._x = FIELD_WIDTH / 2 - 1;
    _this._y = 0;
    _squares = [new Square(color), new Square(color), new Square(color), new Square(color)];
  };

  this.reset = function (offsetX, offsetY) {
    this.clear(offsetX, offsetY);
    _this._x = FIELD_WIDTH / 2 - 1;
    _this._y = 0;
    activateState(0);
  };

  this.defineState = function (stateId, shapeMatrix) {
    _shapes[stateId] = shapeMatrix;
  };

  this.clear = function (offsetX, offsetY) {
    foreachSquare(function (square) { square.clear(offsetX, offsetY); });
  };

  this.draw = function (offsetX, offsetY) {
    foreachSquare(function (square) { square.draw(offsetX, offsetY); });
  };

  this.checkMove = function (deltaX, deltaY) {
    var canMove = true;
    foreachSquare(function (square) {
      if (!square.checkMove(deltaX, deltaY)) {
        canMove = false;
      }
    });

    return canMove;
  };

  this.isTopOverflow = function (deltaY) {
    var hasOverflow = false;
    foreachSquare(function (square) {
      if (square.isTopOverflow(deltaY)) {
        hasOverflow = true;
      }
    });

    return hasOverflow;
  };

  this.move = function (deltaX, deltaY) {
    _this._x += deltaX;
    _this._y += deltaY;
    foreachSquare(function (square) { square.move(deltaX, deltaY); });
  };

  this.dock = function () {
    foreachSquare(function (square) { square.dock(); });
  };

  this.turn = function (direction) {
    var nextStateId = (_currentStateId + (direction == TURN_LEFT ? -1 : 1) + _shapes.length) % _shapes.length;
    if (nextStateId == _currentStateId) {
      // nothing to do, same state
      return;
    }

    if (!_this.checkTurn(_shapes[nextStateId])) {
      // turn not allowed
      return;
    }

    activateState(nextStateId);
  };

  this.checkTurn = function (shapeMatrix) {
    for (var i = 0; i < shapeMatrix.length; i++) {
      var newX = _this._x + shapeMatrix[i][0];
      var newY = _this._y + shapeMatrix[i][1];
      if (newX < 0 || newX >= FIELD_WIDTH
       || newY < 0 || newY >= FIELD_HEIGHT
       || fieldArray[newX][newY] !== null) {
        return false;
      }
    }

    return true;
  };

  this.initializeShape = function () {
    activateState(0);
  };

  function activateState(id) {
    _currentStateId = id;
    var shapeMatrix = _shapes[id];
    foreachSquare(function (square, i) {
      square.setPosition(_this._x + shapeMatrix[i][0], _this._y + shapeMatrix[i][1]);
    });
  }

  function foreachSquare(action) {
    if (!action) return;

    for (var i = 0; i < _squares.length; i++) {
      action(_squares[i], i);
    }
  }
};

var IShape = function () {
  this.base('#7F8C1F');

  this.defineState(0, [[1, 0], [1, 1], [1, 2], [1, 3]]);
  this.defineState(1, [[0, 1], [1, 1], [2, 1], [3, 1]]);

  this.initializeShape();
};
IShape.prototype = new Shape();

var LShape = function () {
  this.base('#EF7F20');

  this.defineState(0, [[1, 0], [1, 1], [1, 2], [2, 2]]);
  this.defineState(1, [[0, 1], [1, 1], [2, 1], [0, 2]]);
  this.defineState(2, [[0, 0], [1, 0], [1, 1], [1, 2]]);
  this.defineState(3, [[0, 1], [1, 1], [2, 1], [2, 0]]);

  this.initializeShape();
};
LShape.prototype = new Shape();

var JShape = function () {
  this.base('#7FA56C');

  this.defineState(0, [[1, 0], [1, 1], [1, 2], [0, 2]]);
  this.defineState(1, [[0, 1], [1, 1], [2, 1], [0, 0]]);
  this.defineState(2, [[2, 0], [1, 0], [1, 1], [1, 2]]);
  this.defineState(3, [[0, 1], [1, 1], [2, 1], [2, 2]]);

  this.initializeShape();
};
JShape.prototype = new Shape();

var OShape = function () {
  this.base('#F2D680');

  this.defineState(0, [[1, 1], [1, 2], [2, 1], [2, 2]]);

  this.initializeShape();
};
OShape.prototype = new Shape();

var ZShape = function () {
  this.base('#213423');

  this.defineState(0, [[2, 0], [2, 1], [1, 1], [1, 2]]);
  this.defineState(1, [[0, 1], [1, 1], [1, 2], [2, 2]]);

  this.initializeShape();
};
ZShape.prototype = new Shape();

var SShape = function () {
  this.base('#195073');

  this.defineState(0, [[1, 0], [1, 1], [2, 1], [2, 2]]);
  this.defineState(1, [[0, 2], [1, 1], [2, 1], [1, 2]]);

  this.initializeShape();
};
SShape.prototype = new Shape();

var TShape = function () {
  this.base('#9FD7C7');

  this.defineState(0, [[1, 0], [0, 1], [1, 1], [2, 1]]);
  this.defineState(1, [[1, 0], [1, 2], [1, 1], [2, 1]]);
  this.defineState(2, [[1, 2], [0, 1], [1, 1], [2, 1]]);
  this.defineState(3, [[1, 0], [1, 2], [1, 1], [0, 1]]);

  this.initializeShape();
};
TShape.prototype = new Shape();
