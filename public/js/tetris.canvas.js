var fieldArray;
var fieldCount;
var abolishedRows;
var createdShapes;
var level;

var initFieldArray = function () {
  fieldArray = new Array(FIELD_WIDTH);
  for (var i = 0; i < FIELD_WIDTH; ++i) {
    fieldArray[i] = new Array(FIELD_HEIGHT);
    for (var j = 0; j < FIELD_HEIGHT; ++j) {
      fieldArray[i][j] = null;
    }
  }

  fieldCount = new Array(FIELD_HEIGHT);
  for (var i = 0; i < FIELD_HEIGHT; ++i) {
    fieldCount[i] = 0;
  }

  abolishedRows = 0;
  $('#rows').html(abolishedRows);
  createdShapes = 0;
  $('#counter').html(createdShapes);
  level = 1;
  $('#level').html(level);
};

var shape = null;
var holdShape = null;
var holdAllowed = true;
var doPause = false;
var canvas;
var ctx;
var currentTimeout;
var currentDelay = 300;

var initKeys = function() {
  $(document).keydown(function (event) {
    if (shape != null) {
      switch (event.keyCode) {
        case KEY_UP:
          // turn
          if (!doPause && shape.checkTurn(TURN_RIGHT)) {
            shape.clear(FIELD_X_OFFSET, FIELD_Y_OFFSET);
            shape.turn();
            shape.draw(FIELD_X_OFFSET, FIELD_Y_OFFSET);
          }
          break;
        case KEY_LEFT:
          // move left 
          if (!doPause && shape.checkMove(-1, 0)) {
            shape.clear(FIELD_X_OFFSET, FIELD_Y_OFFSET);
            shape.move(-1, 0);
            shape.draw(FIELD_X_OFFSET, FIELD_Y_OFFSET);
          }
          break;
        case KEY_RIGHT:
          // move right
          if (!doPause && shape.checkMove(1, 0)) {
            shape.clear(FIELD_X_OFFSET, FIELD_Y_OFFSET);
            shape.move(1, 0);
            shape.draw(FIELD_X_OFFSET, FIELD_Y_OFFSET);
          }
          break;
        case KEY_DOWN:
          // move down
          if (!doPause && shape.checkMove(0, 1)) {
            shape.clear(FIELD_X_OFFSET, FIELD_Y_OFFSET);
            shape.move(0, 1);
            shape.draw(FIELD_X_OFFSET, FIELD_Y_OFFSET);
          }
          break;
        case KEY_HOLD:
          // hold (H) pressed
          putShapeToHold();
          break;
        case KEY_PAUSE:
          // hold (P) pressed
          pause();
          break;
      }
    }
  });
}

$(function () {
  var $canvas = $('#tetris-canvas');
  canvas = $canvas[0];
  ctx = canvas.getContext('2d');

  initKeys();

  $('#link-start').click(function(){
    clickStart();
  });
  $('#link-increase').click(function(){
    doIncrease(1);
  });
});

var clickStart = function() {
  if (currentTimeout) {
    clearTimeout(currentTimeout);
  }

  initFieldArray();
  initPlayfield();
  shape = getShape();
  currentTimeout = delayedTask(startGame, currentDelay);
}

var pause = function() {
  if (doPause) {
    doPause = false;
    startGame(false);
  } else {
    doPause = true;
  }
}

var putShapeToHold = function() {
  if (!holdAllowed) {
    return;
  }

  var tempShape = shape;
  if (holdShape) {
    shape = holdShape;
  } else {
    shape = getShape();
  }

  // re-init position
  tempShape.reset(FIELD_X_OFFSET, FIELD_Y_OFFSET);
  holdShape = tempShape;
  holdAllowed = false;

  ctx.save();
  var left = FIELD_X_OFFSET + SQUARE_WIDTH * (FIELD_WIDTH + 2);
  var right = left + 4 * SQUARE_WIDTH + 1;
  var top = FIELD_Y_OFFSET - 1;
  var bottom = FIELD_Y_OFFSET + SQUARE_WIDTH * 4 + 1;
}

var doIncrease = function(amount) {
  // move up docked squares (don't draw yet, shape needs to be checked first)
  for (var j = 0; j < FIELD_HEIGHT; ++j) {
    if (fieldCount[j] == 0) {
      // nothing to do (yet)
      continue;
    }
    for (var i = 0; i < FIELD_WIDTH; ++i) {
      if (fieldArray[i][j] != null) {
        fieldArray[i][j].clear(FIELD_X_OFFSET, FIELD_Y_OFFSET);
        fieldArray[i][j].move(0, -amount);
        fieldArray[i][j - amount] = fieldArray[i][j];
        fieldArray[i][j] = null;
      }
    }
    fieldCount[j - amount] = fieldCount[j];
    fieldCount[j] = 0;
  }

  // fill empied rows (don't draw yet, shape needs to be checked first)
  var hole = getRandomInt(0, FIELD_WIDTH-1);
  for (var j = FIELD_HEIGHT - 1; j >= FIELD_HEIGHT - amount; --j) {
    for (var i = 0; i < FIELD_WIDTH; ++i) {
      if (i != hole) {
        fieldArray[i][j] = new Square();
        fieldArray[i][j].setPosition(i, j);
      }
    }
    fieldCount[j] = FIELD_WIDTH - 1;
  }

  // move shape up if necessary
  if (shape) {
    var moveY = 0;
    while (!shape.checkMove(0, moveY)) {
      --moveY;
      if (shape.isTopOverflow(moveY)) {
        gameOver();
        return;
      }
    }
    if (moveY < 0) {
      shape.clear(FIELD_X_OFFSET, FIELD_Y_OFFSET);
      shape.move(0, moveY);
      shape.draw(FIELD_X_OFFSET, FIELD_Y_OFFSET);
    }
  }

  // draw moved squares
  for (var j = 0; j < FIELD_HEIGHT; ++j) {
    if (fieldCount[j] == 0) {
      // nothing to do (yet)
      continue;
    }
    for (var i = 0; i < FIELD_WIDTH; ++i) {
      if (fieldArray[i][j] != null) {
        fieldArray[i][j].draw(FIELD_X_OFFSET, FIELD_Y_OFFSET);
      }
    }
  }
}

var downDelay = function() {
  var newLevel = Math.ceil(createdShapes / 20);
  if (newLevel > 10) {
    newLevel = 10;
  }
  if (newLevel != level) {
    level = newLevel;
    $('#level').html(level);
  }
  return currentDelay - Math.floor(250*(level/10));
}

var startGame = function(allowDelay) {
  if (doPause) {
    return;
  }
  if (shape.checkMove(0, 1)) {
    // move
    shape.clear(FIELD_X_OFFSET, FIELD_Y_OFFSET);
    shape.move(0, 1);
    shape.draw(FIELD_X_OFFSET, FIELD_Y_OFFSET);
    currentTimeout = delayedTask(
         function () { startGame(true); },
         downDelay()
    );
  } else if (allowDelay) {
      // delay dock
    currentTimeout = delayedTask(function () { startGame(false); }, 200);
  } else {
    // dock
    shape.dock();
    abolish();
    shape = getShape();
    if (!shape.checkMove(0, 1)) {
      gameOver();
      return;
    }
    shape.draw(FIELD_X_OFFSET, FIELD_Y_OFFSET);
    currentTimeout = delayedTask(function () { startGame(true); }, currentDelay);
  }
}

var abolish = function() {
  var moveDown = 0;
  for (var j = FIELD_HEIGHT - 1; j >= 0 && fieldCount[j] >= 0; --j) {
    if (fieldCount[j] >= FIELD_WIDTH) {
      ++moveDown;
      for (var i = 0; i < FIELD_WIDTH; ++i) {
        fieldArray[i][j].clear(FIELD_X_OFFSET, FIELD_Y_OFFSET);
        fieldArray[i][j] = null;
      }
      fieldCount[j] = 0;
    }
    else if (moveDown > 0) {
      for (var i = 0; i < FIELD_WIDTH; ++i) {
        if (fieldArray[i][j] != null) {
          fieldArray[i][j].clear(FIELD_X_OFFSET, FIELD_Y_OFFSET);
          fieldArray[i][j].move(0, moveDown);
          fieldArray[i][j + moveDown] = fieldArray[i][j];
          fieldArray[i][j] = null;
          fieldArray[i][j + moveDown].draw(FIELD_X_OFFSET, FIELD_Y_OFFSET);
        }
      }
      fieldCount[j + moveDown] = fieldCount[j];
      fieldCount[j] = 0;
    }
  }

  if (moveDown > 0) {
    abolishedRows += moveDown;
    $('#rows').html(abolishedRows);
    if (moveDown > 1) {
      sendDecreased(moveDown - 1);
    }
  }
}

var gameOver = function() {
  clearTimeout(currentTimeout);
  shape = null;
  ctx.save();
  ctx.fillStyle = '#990000';
  ctx.font = 'bold 30px sans-serif';
  var textWidth = ctx.measureText("GAME OVER").width;
  var textX = FIELD_X_OFFSET + (FIELD_WIDTH * SQUARE_WIDTH - textWidth) / 2;
  var textY = FIELD_Y_OFFSET + FIELD_HEIGHT * SQUARE_WIDTH / 2;
  ctx.fillText("GAME OVER", textX, textY);
  ctx.restore();
}

var getShape = function() {
  var r = getRandomInt(0, 6);
  var shape;
  switch (r) {
    case 0:
    default:
      shape = new IShape();
      break;
    case 1:
      shape = new LShape();
      break;
    case 2:
      shape = new OShape();
      break;
    case 3:
      shape = new ZShape();
      break;
    case 4:
      shape = new JShape();
      break;
    case 5:
      shape = new TShape();
      break;
    case 6:
      shape = new SShape();
      break;
  }

  holdAllowed = true;
  ++createdShapes;
  $('#counter').html(createdShapes);
  return shape;
}

var initPlayfield = function() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.save();
  ctx.lineWidth = 1;
  ctx.strokeStyle = '#cccccc';
  ctx.beginPath();
  var left = FIELD_X_OFFSET - 1;
  var right = FIELD_X_OFFSET + SQUARE_WIDTH * FIELD_WIDTH + 1;
  var top = FIELD_Y_OFFSET - 1;
  var bottom = FIELD_Y_OFFSET + SQUARE_WIDTH * FIELD_HEIGHT + 1;

  ctx.moveTo(left, top);
  ctx.lineTo(left, bottom);
  ctx.lineTo(right, bottom);
  ctx.lineTo(right, top);
  ctx.stroke();
  ctx.restore();

  initHoldArea();
}

var initHoldArea = function() {
  ctx.save();
  ctx.lineWidth = 1;
  ctx.strokeStyle = '#cccccc';
  ctx.beginPath();
  var left = FIELD_X_OFFSET + SQUARE_WIDTH * (FIELD_WIDTH + 2);
  var right = left + 4 * SQUARE_WIDTH + 2;
  var top = FIELD_Y_OFFSET - 1;
  var bottom = FIELD_Y_OFFSET + SQUARE_WIDTH * 4 + 2;

  ctx.moveTo(left, top);
  ctx.lineTo(left, bottom);
  ctx.lineTo(right, bottom);
  ctx.lineTo(right, top);
  ctx.stroke();
  ctx.restore();
}
