var abolishSocket = io.connect(document.domain + '/abolish', 'hallo');
var abolishSocketID;

abolishSocket.on('who are you', function (data) {
  abolishSocketID = data.id;
  // TODO get user name
  abolishSocket.emit('check in', {'name': 'david'});
});

abolishSocket.on('someone decreased', function (data) {
  if (data.id != abolishSocketID) {
    // increase if not initiated by myself
    doIncrease(data.amount);
    console.log(data);
  }
});

function sendDecreased(amount) {
  var data = {'amount' : amount };
  abolishSocket.emit('decreased', data);
}
