function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function delayedTask(action) {
    return delayedTask(action, DEFAULT_TASK_DELAY, null);
}

function delayedTask(action, timeout) {
    return delayedTask(action, timeout, null);
}

function delayedTask(action, timeout, condition) {
    if(!timeout) timeout = DEFAULT_TASK_DELAY;
    return setTimeout(function() {
        if(!condition || condition()) {
          action();
        }}, timeout);
}