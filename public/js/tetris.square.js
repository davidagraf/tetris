var Square = function (color) {
  var _x = 0;
  var _y = 0;
  this._color = color;

  this.setPosition = function (x, y) {
    _x = x;
    _y = y;
  };

  this.clear = function (offsetX, offsetY) {
    ctx.clearRect(offsetX + _x * SQUARE_WIDTH, offsetY + _y * SQUARE_WIDTH, SQUARE_WIDTH, SQUARE_WIDTH);
  };

  this.draw = function (offsetX, offsetY) {
    ctx.save();
    ctx.fillStyle = this._color;
    ctx.fillRect(offsetX + _x * SQUARE_WIDTH, offsetY + _y * SQUARE_WIDTH, SQUARE_WIDTH, SQUARE_WIDTH);
    ctx.restore();
  };

  this.checkMove = function (deltaX, deltaY) {
    var newX = _x + deltaX;
    var newY = _y + deltaY;
    return (newX >= 0 && newX < FIELD_WIDTH
         && newY >= 0 && newY < FIELD_HEIGHT
         && fieldArray[newX][newY] == null);
  };

  this.isTopOverflow = function (deltaY) {
    var newY = _y + deltaY;
    return newY < 0;
  };

  this.move = function (deltaX, deltaY) {
    var newX = _x + deltaX;
    var newY = _y + deltaY;
    // console.log("Move square from (" + _x + "," + _y + ") to (" + newX + "," + newY + ")");
    _x = newX;
    _y = newY;
  };

  this.dock = function () {
    fieldArray[_x][_y] = this;
    ++fieldCount[_y];
  };
}
